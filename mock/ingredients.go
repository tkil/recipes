package mock

var ValidIngredients1 = `Dry:
1 1/2 cup AP Flour
1/8 tsp Salt
1 tsp Baking Soda
1/2 tsp Cinnamon (optional)
1/4 tsp Nutmeg (optional)
---

Wet:
2 Banana (ripe)
1/3 cup Butter
73g
3/4 cup Sugar (may use 1/3 less)
150g
1 Egg (beaten)
1 tsp Vanilla`

var InvalidIngredients1 = `Dry:
1 1/2 cup AP Flour
188g
1/8 tsp Salt
1 tsp Baking Soda
1/2 tsp Cinnamon (optional)
1/4 tsp Nutmeg (optional)
---

Wet:
I like food
2 Banana (ripe)
1/3 cup Butter
73g
3/4 cup Sugar (may use 1/3 less)
150g
1 Egg (beaten)
1 tsp Vanilla`

var InvalidIngredients2 = `1 1/2 cups all purpose flour
1 tsp baking powder
1/4 tsp salt
14 ounces diced FIRM peaches, no need to peel (if you don't have a scale try about 2 1/2 cups)
1 stick (1/2 cup) unsalted butter at room temperature
1 cup sugar (plus 2 Tbsp for sprinkling)
3 large eggs, room temperature
2 tsp vanilla extract
1/4 cup sour cream
1/4 cup buttermilk`
