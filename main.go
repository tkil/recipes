package main

import (
	"log"
	"os"
	"kilburn-cookbook/api"
	"kilburn-cookbook/cookbook"
	"kilburn-cookbook/dao"
	"kilburn-cookbook/model"

	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
)

func main() {
	cookbook.LoadConfig(cookbook.CONFIG_FILE)
	loadEnv()
	api.Server()
	sandbox1()
}

func loadEnv() {
	if os.Getenv("PROMOTION_LEVEL") != "production" {
		var err error
		if os.Getenv("PROMOTION_LEVEL") == "local-production" {
			err = godotenv.Load(".env.production")
		} else {
			err = godotenv.Load()
		}
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}
}

func sandbox1() {

	cookbook.Sync()

	// var rs []model.Recipe
	// cookbook.AuditRecipes()
	// rs = cookbook.FetchTaggedRecipes()
	// dao.Read(cookbook.RECIPES_COLLECTION, docId, &rs)

	// cookbook.AuditRecipe("0A543F55-C3E3-431A-91F7-6B943C936FEB-86549-0000AD9BB041A312")
	// cookbook.Sync()
	// docId := model.DocId{ID: "0A543F55-C3E3-431A-91F7-6B943C936FEB-86549-0000AD9BB041A312"}

	// fmt.Printf("%v", cookbook.DateStamp())
	// fmt.Printf("%v", rs)

	// RSync does a reverse sync on everything, just save instead and sync back down.
	// cookbook.RSync()
	// cookbook.RSyncRecipe("B20586DB-5BCC-4289-9D6F-DF9AD023EAD8-64961-0000E6E2349145D0")

	var res = []model.Recipe{}
	filter := bson.D{{"on_favorites", true}}
	cookbook.AuditRecipes()
	dao.ReadBsonFilter(cookbook.RECIPES_COLLECTION, filter, nil, 0, &res)

	// docId := model.DocId{ID: "0A543F55-C3E3-431A-91F7-6B943C936FEB-86549-0000AD9BB041A312"}
	// cookbook.SyncRecipe(docId.ID)
	// dao.Read(cookbook.RECIPES_COLLECTION, docId, &rs)
	// fmt.Printf("%v", res)
}

func sandbox2() {
	// cookbook.Sync()
	// cookbook.RSync()
	// s1 := "2 1/2 cups AP Flour (sifted)"
	// s2 := "1 Egg"
	// s3 := "233g"
	// s2 := "2 1/2 cups AP Flour (sifted)\n313g1 Egg"
	// sA := "1 1/2 cup AP Flour\n188g\n1/8 tsp Salt\n1 tsp Baking Soda\n1/2 tsp Cinnamon (optional)\n1/4 tsp Nutmeg (optional)\n————————\n2 ct Banana (ripe)\n1/3 cup Butter (or Oil)\n73g (55g)\n3/4 cup Sugar (may use 33% less)\n150g\n1 ct Egg (beaten)\n1 tsp Vanilla"
	// sA := mock.ValidIngredients1
	// m1 := cookbook.ExtractMeasure(s1)
	// m2 := cookbook.ExtractMeasure(s2)
	// m3 := cookbook.ExtractMeasure(s3)
	// fmt.Sprintf("%v\n", m1)
	// fmt.Sprintf("%v\n", m2)
	// fmt.Sprintf("%v\n", m3)
	// isValid, errMsg := cookbook.ValidateIngredientLines(sA)
	// errMsg := cookbook.AddMassLines(sA)
	// if !isValid {
	// 	fmt.Println(errMsg)
	// }
}
