package api

import (
	"kilburn-cookbook/cookbook"
	"net/http"
	"os"

	"github.com/labstack/echo"
)

func Server() {
	e := echo.New()
	healthCheckPath := os.Getenv("HEALTH_CHECK_PATH")
	e.GET(healthCheckPath, func(c echo.Context) error {
		return c.String(http.StatusOK, "Server is up.\n")
	})
	e.GET("/audit", func(c echo.Context) error {
		cookbook.Sync()
		cookbook.AuditRecipes()
		return c.String(http.StatusOK, "Sync and audit complete!\n")
	})
	e.GET("/sync", func(c echo.Context) error {
		cookbook.Sync()
		return c.String(http.StatusOK, "Sync complete!\n")
	})
	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT") + ""))
}
