package cookbook

import (
	"fmt"
	"kilburn-cookbook/dao"
	"kilburn-cookbook/model"
	"kilburn-cookbook/paprika"
)

func RSync() {
	rSyncRecipes()
}

func rSyncCategories() {
	docId := model.DocId{ID: METADATA_ID_CATEGORIES}
	var c []model.CategoriesDocument
	dao.Read(METADATA_COLLECTION, docId, &c)
	if len(c) > 0 {
		paprika.SaveCategories(c[0].Categories)
	}
}

func rSyncRecipes() {
	mh := getMasterHashes()
	for _, rh := range mh {
		RSyncRecipe(rh.ID)
	}
}

func RSyncRecipe(id string) {
	docId := model.DocId{ID: id}
	var r []model.Recipe
	dao.Read(RECIPES_COLLECTION, docId, &r)
	if len(r) > 0 {
		h := model.History{LifeCycle: "preRSync", Source: "recipe", SourceID: id}
		addHistory(h)
		fmt.Printf("%v", r[0])
		paprika.SaveRecipe(r[0])
	}
}
