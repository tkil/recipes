package cookbook

import (
	"fmt"
	"kilburn-cookbook/dao"
	"kilburn-cookbook/model"
	"time"
)

func historyId(h model.History) string {
	t := fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02d-00:00\n",
		h.TimeStamp.Year(), h.TimeStamp.Month(), h.TimeStamp.Day(),
		h.TimeStamp.Hour(), h.TimeStamp.Minute(), h.TimeStamp.Second(),
	)
	return fmt.Sprintf("%s::%s::%s", h.Source, h.SourceID, t)
}

func addHistory(h model.History) {
	docId := model.DocId{ID: h.SourceID}
	var res []map[string]interface{}
	h.TimeStamp = time.Now()
	h.ID = historyId(h)
	if h.Source == "recipe" {
		dao.Read(RECIPES_COLLECTION, docId, &res)
	}
	if len(res) > 0 {
		h.Doc = res[0]
		dao.Upsert(HISTORY_COLLECTION, h)
	}
}
