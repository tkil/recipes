package cookbook

import (
	"errors"
	"kilburn-cookbook/model"
	"regexp"
)

func isBlankLine(s string) bool {
	re := regexp.MustCompile(`^$`)
	return re.MatchString(s)
}

func isHeader(s string) bool {
	re := regexp.MustCompile(`^[\w ]+:`)
	return re.MatchString(s)
}

func isHeaderTotaled(s string) bool {
	for _, h := range headerMap {
		if s == h.Name+":" {
			return h.IsMassTotalled
		}
	}
	return true
}

func isHeaderValid(s string) bool {
	for _, h := range headerMap {
		if s == h.Name+":" {
			return true
		}
	}
	return false
}

func isHr(s string) bool {
	re := regexp.MustCompile(`^---$`)
	return re.MatchString(s)
}

func isIngredientAmount(m model.Measure) bool {
	return m.Amount != 0
}

func isKnownIngredient(m model.Measure) bool {
	return m.Ingredient.Name != ""
}

func isMassLine(m model.Measure) bool {
	return m.Ingredient.Name == "" && m.Unit.Name == "g"
}

func isMissingMassLine(lines []string, i int) bool {
	currM := ExtractMeasure(lines[i])
	nextM := model.Measure{}
	if i+1 < len(lines) {
		nextM = ExtractMeasure(lines[i+1])
	}
	if isKnownIngredient(currM) {
		if !isMassLine(nextM) {
			return true
		}
	}
	return false
}

//>---
func isMassCorrect(lines []string, i int) bool {
	currM := ExtractMeasure(lines[i])
	nextM := model.Measure{}
	if i+1 < len(lines) {
		nextM = ExtractMeasure(lines[i+1])
	}
	if isKnownIngredient(currM) {
		if isMassLine(nextM) {
			if isMassLine(nextM) {
				return true
			}
		}
	}
	return false
}

func isNote(s string) bool {
	re := regexp.MustCompile(`^\*.*\*$`)
	return re.MatchString(s)
}

func isUnit(m model.Measure) bool {
	if m.Unit.Name == "" && !m.Ingredient.IsWhole {
		return false
	}
	return true
}

func validateIngredientLine(s string) error {

	if isHr(s) || isBlankLine(s) || isNote(s) {
		return nil
	}

	if isHeader(s) {
		if isHeaderValid(s) {
			return nil
		}
		return errors.New("Invalid Header: \"" + s + "\"")
	}

	m := ExtractMeasure(s)
	if isMassLine(m) {
		return nil
	}

	if !isKnownIngredient(m) {
		return errors.New("Unknown Ingredient: \"" + s + "\"")
	}

	if !isIngredientAmount(m) {
		return errors.New("No amount for: \"" + s + "\"")
	}

	if !isUnit(m) {
		return errors.New("No unit for \"" + s + "\"")
	}

	return nil
}

func ValidateIngredientLines(s string) error {
	r := regexp.MustCompile("\n")
	lines := r.Split(s, -1)
	for i := range lines {
		err := validateIngredientLine(lines[i])
		// fmt.Printf("%v::%v\n", lines[i], isValid)
		if err != nil {
			return err
		}
	}
	return nil
}
