package cookbook

import (
	"fmt"
	"kilburn-cookbook/model"
	"testing"
)

func TestAddMassLines(t *testing.T) {
	LoadConfig("../config.yaml")
	r := model.Recipe{
		Ingredients: `1/2 cup AP Flour
1/2 cup Sugar

Wet:
1/2 cup Milk

Topping:
2 TBL Butter
1/2 cup Powdered Sugar`,
	}
	expected := `1/2 cup AP Flour
62g
1/2 cup Sugar
100g

Wet:
1/2 cup Milk
118g

Topping:
2 TBL Butter
29g
1/2 cup Powdered Sugar
62g`
	_, err := addMassLines(&r)
	if r.Ingredients != expected {
		if err != nil {
			fmt.Println(err)
		}
		// fmt.Println(r.Ingredients)
		// fmt.Println(expected)
		t.Errorf("Mass lines not added.")
	}
}

func TestCorrectMassLines(t *testing.T) {
	LoadConfig("../config.yaml")
	r := model.Recipe{
		Ingredients: `1/2 cup AP Flour
50g
1/2 cup Sugar

Wet:
1/2 cup Milk
100g

Topping:
2 TBL Butter
29g
1/2 cup Powdered Sugar
16g`,
	}
	expected := `1/2 cup AP Flour
62g
1/2 cup Sugar

Wet:
1/2 cup Milk
118g

Topping:
2 TBL Butter
29g
1/2 cup Powdered Sugar
62g`
	_, err := correctMassLines(&r)
	if r.Ingredients != expected {
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(r.Ingredients)
		fmt.Println("---vs---")
		fmt.Println(expected)
		t.Errorf("Mass lines not corrected.")
	}
}

func TestAddMassYield(t *testing.T) {
	LoadConfig("../config.yaml")
	r := model.Recipe{
		Ingredients: `1/2 cup AP Flour
62g
1/2 cup Sugar
100g

Wet:
1/2 cup Milk
118g

Topping:
2 TBL Butter
29g
1/2 cup Powdered Sugar
62g`,
	}
	expected := "Yield: 280g"
	actual := addMassYield(&r)
	if actual != expected {
		// fmt.Println(actual)
		// fmt.Println(expected)
		t.Errorf("Mass yield not correct.")
	}
}

func TestCheckCooktime(t *testing.T) {
	r := model.Recipe{
		CookTime: "20min@350˚",
	}
	err := checkCooktime(r)
	if err != nil {
		fmt.Println(err.Error())
		t.Errorf("Cook Time False Error.")
	}
	r.CookTime = "20min350˚"
	err = checkCooktime(r)
	if err == nil {
		fmt.Printf("%s should be an error...", r.CookTime)
		t.Errorf("Missed Error Error.")
	}
}

func TestFormatIngredient(t *testing.T) {
	LoadConfig("../config.yaml")
	r := model.Recipe{
		Ingredients: `1 1/2 cup all-purpose flour
187g
1/2 cup sugar
1 teaspoon baking soda

Wet:
1/2 cup Milk
118g
1/3 cup Oil

Topping:
2 tablespoons Butter
29g
1/2 cup Oats
50g
1/2 cup Powdered Sugar
62g`,
	}
	expected := `1 1/2 cup AP Flour
187g
1/2 cup Sugar
1 tsp Baking Soda

Wet:
1/2 cup Milk
118g
1/3 cup Canola Oil

Topping:
2 TBL Butter
29g
1/2 cup Rolled Oats
50g
1/2 cup Powdered Sugar
62g`
	_, err := formatIngredientLines(&r)
	if r.Ingredients != expected {
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(r.Ingredients)
		fmt.Println("---vs---")
		fmt.Println(expected)
		t.Errorf("Lines not formatted.")
	}
}

func TestAddMassLogTag(t *testing.T) {
	testFirstTag(t)
	testNextTag(t)
}

func testFirstTag(t *testing.T) {
	var ls []model.Tag
	l := "1/4 cup Stuff"
	m := "50g"
	ls = addMassLogTag(ls, l, m)
	expected := []model.Tag{
		model.Tag{
			Code:    "LOG",
			Message: "Added mass 50g to \"1/4 cup Stuff\"",
		},
	}
	if len(ls) != len(expected) {
		t.Errorf("The lengths do not match.")
	}
	if ls[0].Code != expected[0].Code {
		t.Errorf("The codes do not match.")
	}
	if ls[0].Message != expected[0].Message {
		fmt.Println(ls[0].Message)
		fmt.Println(expected[0].Message)
		t.Errorf("The messages do not match.")
	}
}

func testNextTag(t *testing.T) {

}

func TestClearDescriptionTagSection(t *testing.T) {
	testSectionClear1(t)
	testSectionClear2(t)
	testTagClear(t)
}

func testSectionClear1(t *testing.T) {
	r := model.Recipe{
		Description: "This is in the tag section\n>---\nThe best cake EVERRR",
	}
	expected := model.Recipe{
		Description: "The best cake EVERRR",
	}
	clearDescriptionTagSection(&r)
	if len(r.Description) != len(expected.Description) {
		t.Errorf("The tag was not cleared.")
	}
}

func testSectionClear2(t *testing.T) {
	r := model.Recipe{
		Description: `:REV: Recipe was processed, please review.
>---
`,
	}
	expected := model.Recipe{
		Description: "",
	}
	clearDescriptionTagSection(&r)
	if len(r.Description) != len(expected.Description) {
		fmt.Println(r.Description)
		fmt.Println("---vs---")
		fmt.Println(expected.Description)

		t.Errorf("The tag was not cleared.")
	}
}

func testTagClear(t *testing.T) {
	r := model.Recipe{
		Description: ":sometag: This is a tag item\nThe best cake EVERRR",
	}
	expected := model.Recipe{
		Description: "The best cake EVERRR",
	}
	clearDescriptionTagSection(&r)
	if len(r.Description) != len(expected.Description) {
		fmt.Println(r.Description)
		fmt.Println("---vs---")
		fmt.Println(expected.Description)
		t.Errorf("The tag was not cleared.")
	}
}

func TestInsertLine(t *testing.T) {
	initial := []string{
		"aaa",
		"bbb",
		"ccc",
		"ddd",
	}

	actual := insertLine(initial, "zzz", 2)

	expected := []string{
		"aaa",
		"bbb",
		"zzz",
		"ccc",
		"ddd",
	}

	if len(actual) != len(expected) {
		t.Errorf("The slice lengths do not match.")
	}
	if actual[2] != expected[2] {
		t.Errorf("The new item is not in the correct position.")
	}
}
