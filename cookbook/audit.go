package cookbook

import (
	"fmt"
	"kilburn-cookbook/dao"
	"kilburn-cookbook/model"
	"kilburn-cookbook/paprika"
	"regexp"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func clearReviewedRecipes() {
	var rs = []model.Recipe{}
	filter := bson.D{
		{"description", primitive.Regex{Pattern: "^:REV:.*\n>---"}},
		{"on_favorites", false},
	}
	sort := bson.D{
		{"created", 1},
	}
	dao.ReadBsonFilter(RECIPES_COLLECTION, filter, sort, 0, &rs)

	for _, r := range rs {
		clearDescriptionTagSection(&r)
		paprika.SaveRecipe(r)
		fmt.Printf("Cleared %s : %s\n", r.Name, r.ID)
	}
}

func FetchTaggedAudRecipes() []model.Recipe {
	var res = []model.Recipe{}
	filter := bson.D{
		{"name", primitive.Regex{Pattern: "^(?!_).*"}},            //Ignore _ prefix
		{"name", primitive.Regex{Pattern: "^(?!zz).*"}},           //Ignore ZZ prefix
		{"description", primitive.Regex{Pattern: "^(?!:REV:).*"}}, //Ignore REV tag
		{"description", primitive.Regex{Pattern: ">---|^:[Aa][uU][dD]:"}},
		{"on_favorites", false},
	}
	sort := bson.D{
		{"created", 1},
	}
	dao.ReadBsonFilter(RECIPES_COLLECTION, filter, sort, 99, &res)
	for i := range res {
		re := regexp.MustCompile(":[REV]*: .*\n*>---\n{0,1}")
		if re.MatchString(res[i].Description) {
			res = append(res[:i], res[i+1:]...)
		}
	}
	return res
}

func FetchTaggedRecipes(limit int) []model.Recipe {
	var res = []model.Recipe{}
	filter := bson.D{
		{"name", primitive.Regex{Pattern: "^(?!_).*"}},            //Ignore _ prefix
		{"name", primitive.Regex{Pattern: "^(?!zz).*"}},           //Ignore ZZ prefix
		{"description", primitive.Regex{Pattern: "^(?!:REV:).*"}}, //Ignore REV tag
		{"description", primitive.Regex{Pattern: ">---|^:(.*){3}:"}},
		{"on_favorites", false},
	}
	sort := bson.D{
		{"created", 1},
	}
	dao.ReadBsonFilter(RECIPES_COLLECTION, filter, sort, limit, &res)
	for i := range res {
		re := regexp.MustCompile(":[REV]*: .*\n*>---\n{0,1}")
		if re.MatchString(res[i].Description) {
			res = append(res[:i], res[i+1:]...)
		}
	}
	return res
}

func FetchUntaggedRecipes(limit int) []model.Recipe {
	var res = []model.Recipe{}
	filter := bson.D{
		{"name", primitive.Regex{Pattern: "^(?!_).*"}},            //Ignore _ prefix
		{"name", primitive.Regex{Pattern: "^(?!zz).*"}},           //Ignore ZZ prefix
		{"description", primitive.Regex{Pattern: "^(?!:REV:).*"}}, //Ignore REV tag
		{"on_favorites", false},
	}
	sort := bson.D{
		{"created", 1},
	}
	dao.ReadBsonFilter(RECIPES_COLLECTION, filter, sort, limit, &res)
	return res
}

func GetRecipesToAudit() []model.Recipe {
	var recs []model.Recipe
	fc := getFavoriteCount()
	pullLimit := auditLimit - fc
	fmt.Printf("Audit limit: %d\n", auditLimit)
	fmt.Printf("Recipes under review: %d\n", auditLimit)
	fmt.Printf("Recipes to pull: %d\n", pullLimit)
	audRecs := FetchTaggedAudRecipes()
	recs = append(recs, audRecs...)
	if pullLimit > 0 {
		tagged := FetchTaggedRecipes(pullLimit)
		pullLimit = pullLimit - len(tagged)
		recs = append(recs, tagged...)
	}
	if pullLimit > 0 {
		untagged := FetchUntaggedRecipes(pullLimit)
		recs = append(recs, untagged...)
	}
	return recs
}

func AuditRecipes() {
	clearReviewedRecipes()
	recs := GetRecipesToAudit()
	for _, r := range recs {
		fmt.Printf("Auditing %s : %s\n", r.Name, r.ID)
		AuditRecipe(r.ID)
	}
}

func logErrorAndSave(r model.Recipe, err error) {
	SetDescriptionTags(&r, messageToTags("VAL", err.Error()))
	r.OnFavorites = true
	fmt.Printf("Found issue with %s : %s", r.Name, r.ID)
	paprika.SaveRecipe(r)
}

func AuditRecipe(id string) {
	docId := model.DocId{id}
	var res = []model.Recipe{}
	dao.Read(RECIPES_COLLECTION, docId, &res)
	r := res[0]
	tags, err := addMassLines(&r)
	if err != nil {
		logErrorAndSave(r, err)
		return
	}
	tagsNew, err := correctMassLines(&r)
	if err != nil {
		logErrorAndSave(r, err)
		return
	}
	tags = append(tags, tagsNew...)
	tagsNew, err = formatIngredientLines(&r)
	if err != nil {
		logErrorAndSave(r, err)
		return
	}
	err = checkCooktime(r)
	if err != nil {
		logErrorAndSave(r, err)
		return
	}
	tags = append(tags, tagsNew...)
	sigMass := addMassYield(&r)
	if r.Servings != sigMass {
		tags = append(
			tags,
			messageToTags("LOG", fmt.Sprintf("Updated mass yield to %s.", sigMass))...,
		)
	}

	tags = append(
		messageToTags("REV", "Recipe was processed, please review."),
		tags...,
	)

	SetDescriptionTags(&r, tags)
	fmt.Printf("%v", r.Description)
	r.OnFavorites = true
	fmt.Printf("Processed %s : %s", r.Name, r.ID)
	paprika.SaveRecipe(r)
}

func getFavoriteCount() int {
	var res = []model.Recipe{}
	filter := bson.D{
		{"name", primitive.Regex{Pattern: "^(?!_).*"}}, //Ignore _ prefix
		{"on_favorites", true},
	}
	dao.ReadBsonFilter(RECIPES_COLLECTION, filter, nil, 0, &res)
	return len(res)
}

func messageToTags(code string, msg string) []model.Tag {
	return []model.Tag{model.Tag{Code: code, Message: msg}}
}
