package cookbook

import (
	"kilburn-cookbook/dao"
	"kilburn-cookbook/model"
	"kilburn-cookbook/paprika"
)

func getRecipesToSync(master []model.RecipeHash, stored []model.RecipeHash) []string {
	var ids []string
	storedMap := recipeIdHashMapper(stored)
	for _, m := range master {
		if storedMap[m.UID+":"+m.Hash] != m.UID {
			ids = append(ids, m.UID)
		}
	}
	return ids
}

func Sync() {
	syncRecipeHashes()
	syncRecipes()
	syncCategories()
}

func syncCategories() {
	c, _ := paprika.FetchCategories()
	cd := model.CategoriesDocument{
		ID:         METADATA_ID_CATEGORIES,
		Categories: c,
	}
	dao.Upsert(METADATA_COLLECTION, cd)
}

func syncRecipeHashes() {
	rhs, _ := paprika.FetchRecipeHashes()
	rhd := model.RecipeHashDocument{
		ID:           METADATA_ID_RECIPE_HASHES,
		RecipeHashes: rhs,
	}
	dao.Upsert(METADATA_COLLECTION, rhd)
}

func syncRecipes() {
	mh := getMasterHashes()
	sh := getStoredHashes()
	toUpdate := getRecipesToSync(mh, sh)
	toPrune := getRecipesToPrune(mh, sh)
	// Upsert New and Updated
	for _, id := range toUpdate {
		syncRecipe(id)
	}
	// Prune Missing
	for _, id := range toPrune {
		dao.Delete(RECIPES_COLLECTION, model.DocId{ID: id})
	}
}

func SyncRecipe(id string) {
	syncRecipe(id)
}

func syncRecipe(id string) {
	r, _ := paprika.FetchRecipe(id)
	h := model.History{LifeCycle: "preSync", Source: "recipe", SourceID: id}
	addHistory(h)
	dao.Upsert(RECIPES_COLLECTION, r)
}
