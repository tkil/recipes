package cookbook

import (
	"errors"
	"fmt"
	"kilburn-cookbook/model"
	"math"
	"math/big"
	"regexp"
	"strings"

	"github.com/Knetic/govaluate"
)

// func MassDiscrepancy(r *model.Recipe) string {
// 	var log []model.Tag
// 	re := regexp.MustCompile("\n")
// 	lines := re.Split(r.Ingredients, -1)
// 	for i := range lines {
// 		if !isMissingMassLine(lines, i) {
// 			calcMassLine := calculatedMassLine(lines[i])
// 			m := ExtractMeasure(lines[i+1])
// 			m.Amount

// 			//here
// 		}
// 	}
// 	r.Ingredients = strings.Join(lines, "\n")
// 	return log, nil
// }

func addMassLines(r *model.Recipe) ([]model.Tag, error) {
	var log []model.Tag
	err := ValidateIngredientLines(r.Ingredients)
	if err != nil {
		return log, err
	}
	re := regexp.MustCompile("\n")
	lines := re.Split(r.Ingredients, -1)
	lineLen := len(lines)
	for i := 0; i < lineLen; i++ {
		if isMissingMassLine(lines, i) {
			newMassLine := calculatedMassLine(ExtractMeasure(lines[i]))
			lines = insertLine(lines, newMassLine, i+1)
			log = addMassLogTag(log, lines[i], newMassLine)
			lineLen = len(lines)
		}
	}
	r.Ingredients = strings.Join(lines, "\n")
	return log, nil
}

func correctMassLines(r *model.Recipe) ([]model.Tag, error) {
	var log []model.Tag
	err := ValidateIngredientLines(r.Ingredients)
	if err != nil {
		return log, err
	}
	re := regexp.MustCompile("\n")
	lines := re.Split(r.Ingredients, -1)
	lineLen := len(lines)
	for i := 0; i < lineLen-1; i++ {
		currM := ExtractMeasure(lines[i])
		nextM := ExtractMeasure(lines[i+1])
		if isKnownIngredient(currM) && isMassLine(nextM) {
			actualMass := calculatedMassLine(currM)
			listedMass := lines[i+1]
			if actualMass != listedMass {
				log = addMassCorrectionLogTag(log, lines[i], listedMass, actualMass)
				lines[i+1] = actualMass
			}
		}
	}
	r.Ingredients = strings.Join(lines, "\n")
	return log, nil
}

func formatIngredientLines(r *model.Recipe) ([]model.Tag, error) {
	var log []model.Tag
	err := ValidateIngredientLines(r.Ingredients)
	if err != nil {
		return log, err
	}
	re := regexp.MustCompile("\n")
	lines := re.Split(r.Ingredients, -1)
	lineLen := len(lines)
	for i := 0; i < lineLen; i++ {
		m := ExtractMeasure(lines[i])
		if isKnownIngredient(m) && !isMassLine(m) {

			formattedIngredient := formatIngredient(m)
			if lines[i] != formattedIngredient {
				log = addFormatCorrectionLogTag(log, lines[i], formattedIngredient)
				lines[i] = formattedIngredient
			}
		}
	}
	r.Ingredients = strings.Join(lines, "\n")
	return log, nil
}

func formatIngredient(m model.Measure) string {
	rat := new(big.Rat).SetFloat64(m.Amount)
	bNum := *rat.Num()
	num := bNum.Int64()
	bDen := *rat.Denom()
	den := bDen.Int64()
	quo := num / den
	rem := num % den
	remFrac := approxFraction(rem, den)
	fractionFormat := ""
	if quo > 0 && rem > 0 {
		fractionFormat = fmt.Sprintf("%d %s", quo, remFrac)
	} else if quo > 0 {
		fractionFormat = fmt.Sprintf("%d", quo)
	} else if rem > 0 {
		fractionFormat = fmt.Sprintf("%s", remFrac)
	}
	attributes := ""
	if m.Attributes != "" {
		attributes = fmt.Sprintf(" (%s)", m.Attributes)
	}
	return fmt.Sprintf("%s %s %s%s", fractionFormat, m.Unit.Name, m.Ingredient.Name, attributes)
}

func approxFraction(rem int64, den int64) string {
	if math.Abs(float64(rem)/float64(den)-1.0/3) < 0.01 {
		return "1/3"
	}
	if math.Abs(float64(rem)/float64(den)-2.0/3) < 0.01 {
		return "2/3"
	}
	return fmt.Sprint(new(big.Rat).SetFloat64(float64(rem) / float64(den)))
}

func addMassYield(r *model.Recipe) string {
	re := regexp.MustCompile("\n")
	lines := re.Split(r.Ingredients, -1)
	totalMass := 0.0
	isTotallingSection := true
	for i := range lines {
		if isHeader(lines[i]) {
			isTotallingSection = isHeaderTotaled(lines[i])
		}
		m := ExtractMeasure(lines[i])
		if isMassLine(m) && isTotallingSection {
			totalMass = totalMass + m.Amount
		}
	}
	servings := fmt.Sprintf("Yield: %dg", int(totalMass))
	r.Servings = servings
	return servings
}

func addMassLogTag(ls []model.Tag, l string, m string) []model.Tag {
	return append(ls, model.Tag{
		Code:    "LOG",
		Message: fmt.Sprintf("Added mass %s to \"%s\"", m, l),
	})
}

func addMassCorrectionLogTag(ls []model.Tag, l string, m1 string, m2 string) []model.Tag {
	return append(ls, model.Tag{
		Code:    "LOG",
		Message: fmt.Sprintf("Corrected mass for \"%s\" from %s to %s", l, m1, m2),
	})
}

func addFormatCorrectionLogTag(ls []model.Tag, s1 string, s2 string) []model.Tag {
	return append(ls, model.Tag{
		Code:    "LOG",
		Message: fmt.Sprintf("Formatted corrected from \"%s\" to \"%s\"", s1, s2),
	})
}

func checkCooktime(r model.Recipe) error {
	if r.CookTime == "." {
		return nil
	}
	re := regexp.MustCompile("^[0-9]+[mh]@.+$")
	if re.MatchString(r.CookTime) {
		return nil
	}
	return errors.New("Invalid Cook Time: \"" + r.CookTime + "\"")
}

func calculatedMassLine(m model.Measure) string {
	mass := m.Amount * m.Unit.Value * m.Ingredient.PhysicalDensity
	mass = math.Round(mass*100) / 100
	if mass < 10 {
		return fmt.Sprintf("%.2gg", mass)
	}
	return fmt.Sprintf("%.0fg", mass)
}

func clearDescriptionTagSection(r *model.Recipe) {
	re := regexp.MustCompile("(\n*.)*\n*>---\n{0,1}")
	r.Description = re.ReplaceAllString(r.Description, "")
	re = regexp.MustCompile(":[a-zA-Z0-9]*: .*\n")
	r.Description = re.ReplaceAllString(r.Description, "")
}

func evalAmount(s string) float64 {
	strAmount := strings.Replace(s, " ", "+", -1)
	expression, _ := govaluate.NewEvaluableExpression(strAmount)
	amount, _ := expression.Evaluate(nil)
	return amount.(float64)
}

func ExtractMeasure(s string) model.Measure {
	var m model.Measure
	// Amount
	reAmount := regexp.MustCompile(`^[.\d/\ ]+`)
	resAmount := strings.Trim(reAmount.FindString(s), " ")
	s = strings.Trim(strings.Replace(s, resAmount, "", -1), " ")
	if resAmount == "" {
		resAmount = "0"
	}
	m.Amount = evalAmount(resAmount)
	// Unit
	reUnit := regexp.MustCompile(`^\w*`)
	resUnit := reUnit.FindString(s)
	m.Unit = unitMap[strings.ToLower(resUnit)]
	if m.Unit.Name == "" {
		m.Unit = unitMap[strings.ToLower(resUnit)]
	}
	if m.Unit.Name != "" {
		s = strings.Trim(strings.Replace(s, resUnit, "", -1), " ")
	}
	// Attributes
	reAttributes := regexp.MustCompile(`\(.*\)`)
	resAttributes := reAttributes.FindString(s)
	s = strings.Trim(strings.Replace(s, resAttributes, "", -1), " ")
	m.Attributes = strings.Replace(strings.Replace(resAttributes, "(", "", -1), ")", "", -1)
	// Ingredient
	m.Ingredient = ingredientMap[strings.ToLower(s)]
	if m.Ingredient.Name == "" {
		m.Ingredient = ingredientMap[strings.ToLower(s)]
	}
	if m.Ingredient.IsWhole {
		m.Unit = model.Unit{Value: 1}
	}
	return m
}

func insertLine(lines []string, item string, pos int) []string {
	return append(lines[:pos], append([]string{item}, lines[pos:]...)...)
}

func SetDescriptionTags(r *model.Recipe, tags []model.Tag) {
	clearDescriptionTagSection(r)
	tagSection := ""
	for _, t := range tags {
		tagSection = tagSection + fmt.Sprintf(":%s: %s\n", t.Code, t.Message)
	}
	r.Description = tagSection + ">---\n" + r.Description
}
