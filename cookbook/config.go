package cookbook

import (
	"fmt"
	"io/ioutil"
	"kilburn-cookbook/model"
	"os"
	"strings"

	"gopkg.in/yaml.v2"
)

const HISTORY_COLLECTION = "history"
const METADATA_COLLECTION = "metadata"
const METADATA_ID_CATEGORIES = "categories"
const METADATA_ID_INGREDIENTS = "ingredients"
const METADATA_ID_RECIPE_HASHES = "recipeHashes"
const RECIPES_COLLECTION = "recipe"
const CONFIG_FILE = "./config.yaml"

var containerMap map[string]model.Container
var ingredientMap map[string]model.Ingredient
var unitMap map[string]model.Unit
var headerMap map[string]model.Header
var auditLimit int

func LoadConfig(file string) {
	config := loadConfigFile(file)
	mapContainers(config.Containers)
	mapIngredients(config.Ingredients)
	mapUnits(config.Units)
	mapHeaders(config.Headers)
	loadAuditLimit(config.AuditLimit)
}

func loadAuditLimit(audLimit int) {
	auditLimit = audLimit
}

func loadConfigFile(file string) model.Config {
	var config model.Config
	yamlFile, err := os.Open(file)
	if err != nil {
		fmt.Println(err)
	}
	defer yamlFile.Close()
	byteValue, _ := ioutil.ReadAll(yamlFile)
	yaml.Unmarshal(byteValue, &config)
	return config
}

func mapHeaders(headers []model.Header) {
	headerMap = make(map[string]model.Header)
	for _, h := range headers {
		key := strings.ToLower(h.Name)
		headerMap[key] = h
	}
}

func mapContainers(containers []model.Container) {
	containerMap = make(map[string]model.Container)
	for _, c := range containers {
		key := strings.ToLower(c.Name)
		containerMap[key] = c
	}
}

func mapIngredients(ingredients []model.Ingredient) {
	ingredientMap = make(map[string]model.Ingredient)
	for _, c := range ingredients {
		key := strings.ToLower(c.Name)
		ingredientMap[key] = c
		ingredientMap[key+"s"] = c
		for _, a := range c.Aliases {
			aKey := strings.ToLower(a)
			ingredientMap[aKey] = c
			ingredientMap[aKey+"s"] = c
		}
	}
}

func mapUnits(units []model.Unit) {
	unitMap = make(map[string]model.Unit)
	for _, c := range units {
		key := strings.ToLower(c.Name)
		unitMap[key] = c
		unitMap[key+"s"] = c
		for _, a := range c.Aliases {
			aKey := strings.ToLower(a)
			unitMap[aKey] = c
			unitMap[aKey+"s"] = c
		}
	}
}
