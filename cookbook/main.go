package cookbook

import (
	"context"
	"kilburn-cookbook/dao"
	"kilburn-cookbook/model"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func connect() (mongo.Client, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://root:letmein@localhost:27017"))
	err = client.Connect(ctx)
	return *client, err
}

func getMasterHashes() []model.RecipeHash {
	docId := model.DocId{ID: METADATA_ID_RECIPE_HASHES}
	var rhd []model.RecipeHashDocument
	dao.Read(METADATA_COLLECTION, docId, &rhd)
	if len(rhd) > 0 {
		rhs := rhd[0].RecipeHashes
		for i := range rhs {
			rhs[i].ID = rhs[i].UID
		}
		return rhd[0].RecipeHashes
	}
	return nil
}

func getRecipesToPrune(master []model.RecipeHash, stored []model.RecipeHash) []string {
	var ids []string
	masterMap := recipeIdMapper(master)
	for _, s := range stored {
		if masterMap[s.UID] != s.UID {
			ids = append(ids, s.UID)
		}
	}
	return ids
}

func getStoredHashes() []model.RecipeHash {
	var rhs []model.RecipeHash
	dao.Read(RECIPES_COLLECTION, nil, &rhs)
	return rhs
}

func recipeIdMapper(rhs []model.RecipeHash) map[string]string {
	rim := map[string]string{}
	for _, rh := range rhs {
		rim[rh.UID] = rh.UID
	}
	return rim
}

func recipeIdHashMapper(rhs []model.RecipeHash) map[string]string {
	rihm := map[string]string{}
	for _, rh := range rhs {
		rihm[rh.UID+":"+rh.Hash] = rh.UID
	}
	return rihm
}
