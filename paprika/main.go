package paprika

import (
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"kilburn-cookbook/model"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"time"
)

type CategoriesRespBody struct {
	Result []model.Category `json:"result"`
}
type RecipeHashRespBody struct {
	Result []model.RecipeHash `json:"result"`
}

type RecipeRespBody struct {
	Result model.Recipe `json:"result"`
}

func paprikaURI() string {
	return os.Getenv("PAPRIKA_URI")
}

func basicAuth() string {
	username := os.Getenv("PAPRIKA_USER")
	password := os.Getenv("PAPRIKA_PW")
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func FetchCategories() ([]model.Category, error) {
	apiUrl := paprikaURI() + "/categories"
	client := &http.Client{}
	req, err := http.NewRequest("GET", apiUrl, nil)
	req.Header.Add("Authorization", "Basic "+basicAuth())

	resp, err := client.Do(req)
	body, err := ioutil.ReadAll(resp.Body)

	var data CategoriesRespBody

	json.Unmarshal([]byte(body), &data)
	for i := range data.Result {
		data.Result[i].ID = data.Result[i].UID
	}
	return data.Result, err
}

func FetchRecipeHashes() ([]model.RecipeHash, error) {
	apiUrl := paprikaURI() + "/recipes"

	client := &http.Client{}
	req, err := http.NewRequest("GET", apiUrl, nil)
	req.Header.Add("Authorization", "Basic "+basicAuth())

	resp, err := client.Do(req)
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		panic("omg fires: " + err.Error())
	}

	var data RecipeHashRespBody

	json.Unmarshal([]byte(body), &data)
	for i := range data.Result {
		data.Result[i].ID = data.Result[i].UID
	}
	return data.Result, err
}

func FetchRecipe(uui string) (model.Recipe, error) {
	apiUrl := paprikaURI() + "/recipe" + "/" + uui

	client := &http.Client{}
	req, err := http.NewRequest("GET", apiUrl, nil)
	req.Header.Add("Authorization", "Basic "+basicAuth())

	resp, err := client.Do(req)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic("omg fires: " + err.Error())
	}

	var data RecipeRespBody
	err = json.Unmarshal([]byte(body), &data)
	data.Result.ID = data.Result.UID
	return data.Result, err
}

func SaveCategories(c []model.Category) error {
	apiUrl := paprikaURI() + "/categories" + "/"

	j, err := json.Marshal(c)
	jStr := string(j)
	z, err := gZipData([]byte(jStr))
	client := &http.Client{}

	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	w2, err := w.CreateFormFile("data", "-")
	w2.Write(z)

	w.Close()

	req, err := http.NewRequest("POST", apiUrl, &b)

	req.Header.Set("Content-Type", w.FormDataContentType())
	req.Header.Add("Authorization", "Basic "+basicAuth())

	client.Do(req)

	if err != nil {
		panic(err)
	}
	return nil
}

func hashDoc(i interface{}) string {
	json, _ := json.Marshal(i)
	return strings.ToUpper(fmt.Sprintf("%x", md5.Sum(json)))
}

func dateStamp() string {
	t := time.Now()
	return fmt.Sprintf(t.Format("2006-01-02 15:04:05"))
}

func SaveRecipe(r model.Recipe) error {
	apiUrl := paprikaURI() + "/recipe" + "/" + r.UID + "/"
	r.Created = dateStamp()
	r.Hash = hashDoc(r)

	j, err := json.Marshal(r)
	jStr := string(j)
	z, err := gZipData([]byte(jStr))
	client := &http.Client{}

	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	w2, err := w.CreateFormFile("data", "-")
	w2.Write(z)

	w.Close()

	req, err := http.NewRequest("POST", apiUrl, &b)

	req.Header.Set("Content-Type", w.FormDataContentType())
	req.Header.Add("Authorization", "Basic "+basicAuth())

	client.Do(req)

	if err != nil {
		panic(err)
	}
	return nil
}

func gZipData(data []byte) (compressedData []byte, err error) {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)

	_, err = gz.Write(data)
	if err != nil {
		return
	}

	if err = gz.Flush(); err != nil {
		return
	}

	if err = gz.Close(); err != nil {
		return
	}

	compressedData = b.Bytes()

	return
}
