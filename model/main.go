package model

import "time"

type CategoriesDocument struct {
	ID         string     `bson:"_id" json:"_id"`
	Categories []Category `bson:"categories" bson:"categories"`
}

type Category struct {
	ID        string `bson:"_id" json:"_id"`
	UID       string `bson:"uid" json:"uid"`
	ParentUID string `bson:"parent_uid" json:"parent_uid"`
	OrderFlag int32  `bson:"order_flag" json:"order_flag"`
	Name      string `bson:"name" json:"name"`
}

type Config struct {
	AuditLimit  int          `json:"auditLimit" yaml:"auditLimit"`
	Containers  []Container  `json:"containers" yaml:"containers"`
	Headers     []Header     `json:"headers" yaml:"headers"`
	Ingredients []Ingredient `json:"ingredients" yaml:"ingredients"`
	Units       []Unit       `json:"units" yaml:"units"`
}

type Container struct {
	Name           string `json:"name" yaml:"name"`
	BakingCapacity int    `json:"bakingCapacity" yaml:"bakingCapacity"`
	Capacity       int    `json:"capacity" yaml:"capacity"`
}

type DocId struct {
	ID string `bson:"_id" json:"_id"`
}

type Header struct {
	Name           string `json:"name" yaml:"name"`
	IsMassTotalled bool   `json:"isMassTotalled" yaml:"isMassTotalled"`
}

type History struct {
	ID        string                 `bson:"_id" json:"_id"`
	Source    string                 `bson:"source" json:"source"`
	SourceID  string                 `bson:"sourceID" json:"sourceID"`
	LifeCycle string                 `bson:"lifeCycle" json:"lifeCycle"`
	Doc       map[string]interface{} `bson:"doc" json:"doc"`
	TimeStamp time.Time              `bson:"timeStamp" json:"timeStamp"`
}

type Ingredient struct {
	Name            string    `json:"name" yaml:"name"`
	Aliases         []string  `json:"aliases" yaml:"aliases"`
	PhysicalDensity float64   `json:"physicalDensity" yaml:"physicalDensity"`
	IsWhole         bool      `json:"isWhole" yaml:"isWhole"`
	Nutrition       Nutrition `json:"nutrition" yaml:"nutrition"`
}

type Measure struct {
	Amount     float64
	Unit       Unit
	Ingredient Ingredient
	Attributes string
}

type Nutrition struct {
	CalorieDensity      float64 `json:"calorieDensity" yaml:"calorieDensity"`
	CarbohydrateDensity int     `json:"carbohydrateDensity" yaml:"carbohydrateDensity"`
	ProteinDensity      int     `json:"proteinDensity" yaml:"proteinDensity"`
	FatDensity          int     `json:"fatDensity" yaml:"fatDensity"`
}

type Recipe struct {
	ID              string      `bson:"_id" json:"_id"`
	UID             string      `bson:"uid" json:"uid"`
	Hash            string      `bson:"hash" json:"hash"`
	Rating          int         `bson:"rating" json:"rating"`
	PhotoHash       string      `bson:"photo_hash" json:"photo_hash"`
	OnFavorites     bool        `bson:"on_favorites" json:"on_favorites"`
	Photo           string      `bson:"photo" json:"photo"`
	Scale           interface{} `bson:"scale" json:"scale"`
	Ingredients     string      `bson:"ingredients" json:"ingredients"`
	IsPinned        bool        `bson:"is_pinned" json:"is_pinned"`
	Source          string      `bson:"source" json:"source"`
	TotalTime       string      `bson:"total_time" json:"total_time"`
	Description     string      `bson:"description" json:"description"`
	SourceURL       string      `bson:"source_url" json:"source_url"`
	Difficulty      string      `bson:"difficulty" json:"difficulty"`
	OnGroceryList   interface{} `bson:"on_grocery_list" json:"on_grocery_list"`
	InTrash         bool        `bson:"in_trash" json:"in_trash"`
	Directions      string      `bson:"directions" json:"directions"`
	Categories      []string    `bson:"categories" json:"categories"`
	PhotoURL        string      `bson:"photo_url" json:"photo_url"`
	CookTime        string      `bson:"cook_time" json:"cook_time"`
	Name            string      `bson:"name" json:"name"`
	Created         string      `bson:"created" json:"created"`
	Notes           string      `bson:"notes" json:"notes"`
	PhotoLarge      interface{} `bson:"photo_large" json:"photo_large"`
	ImageURL        string      `bson:"image_url" json:"image_url"`
	PrepTime        string      `bson:"prep_time" json:"prep_time"`
	Servings        string      `bson:"servings" json:"servings"`
	NutritionalInfo string      `bson:"nutritional_info" json:"nutritional_info"`
}

type RecipeHash struct {
	ID   string `bson:"_id" json:"_id"`
	UID  string `bson:"uid" json:"uid"`
	Hash string `bson:"hash" json:"hash"`
}

type RecipeHashDocument struct {
	ID           string       `bson:"_id" json:"_id"`
	RecipeHashes []RecipeHash `bson:"recipeHashes" bson:"recipeHashes"`
}

type Tag struct {
	Code    string
	Message string
}

type Unit struct {
	Name    string   `json:"name" yaml:"name"`
	Aliases []string `json:"aliases" yaml:"aliases"`
	Value   float64  `json:"value" yaml:"value"`
}
