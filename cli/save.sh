#!/bin/bash
USER=$(cat ./cli/.env | grep USER= | head -n 1 | cut -d= -f2)
# sh ./curl/get.sh
echo '----'
cat ./cli/sample.txt | gzip | curl -X POST \
  --header 'Content-Type: multipart/form-data' \
  --user "$USER" \
  --form data=@- \
  https://www.paprikaapp.com/api/v1/sync/recipe/0A543F55-C3E3-431A-91F7-6B943C936FEB-86549-0000AD9BB041A312/
  
# cat ./curl/sample.txt | gzip | curl -X POST \
#   --header 'Content-Type: multipart/form-data' \
#   --user "$USER" \
#   --form data=@- \
#   https://postman-echo.com/post

# sh ./curl/get.sh
