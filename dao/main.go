package dao

import (
	"context"
	"fmt"
	"kilburn-cookbook/model"
	"log"
	"os"
	"time"

	"github.com/mitchellh/mapstructure"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func databaseName() string {
	return os.Getenv("MONGO_DATABASE")
}

func connect() (mongo.Client, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	mongoHost := os.Getenv("MONGO_HOST")
	mongoPort := os.Getenv("MONGO_PORT")
	mongoAuthDatabase := os.Getenv("MONGO_AUTH_DATABASE")
	mongoUser := os.Getenv("MONGO_USER")
	mongoPW := os.Getenv("MONGO_PW")
	mongoURI := "mongodb://" + mongoUser + ":" + mongoPW + "@" + mongoHost + ":" + mongoPort + "/" + mongoAuthDatabase
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	err = client.Connect(ctx)
	return *client, err
}

func Delete(collection string, doc interface{}) {
	client, err := connect()
	col := client.Database(databaseName()).Collection(collection)
	if err != nil {
		panic("omg fires: " + err.Error())
	}

	var docId model.DocId
	mapstructure.Decode(doc, &docId)

	col.DeleteOne(
		context.Background(),
		bson.D{{"_id", docId.ID}},
	)
}

func Read(collection string, filter interface{}, result interface{}) {
	var bFilter interface{}
	if filter == nil {
		bFilter = bson.M{}
	} else {
		bMarsh, err := bson.Marshal(filter)
		if err != nil {
			log.Fatal(err)
		}
		bFilter = bMarsh
	}

	ReadBsonFilter(collection, bFilter, nil, 0, result)
}

func ReadBsonFilter(collection string, bFilter interface{}, sort interface{}, limit int, result interface{}) {
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	client, err := connect()
	col := client.Database(databaseName()).Collection(collection)
	if err != nil {
		panic("omg fires: " + err.Error())
	}

	var cur *mongo.Cursor

	options := options.FindOptions{}
	if sort != nil {
		options.Sort = sort
	}
	if limit != 0 {
		i64 := int64(limit)
		options.Limit = &i64
	}

	cur, err = col.Find(context.Background(), bFilter, &options)
	defer cur.Close(ctx)

	var data []interface{}
	for cur.Next(ctx) {
		var datum map[string]interface{}
		err = cur.Decode(&datum)
		if err != nil {
			log.Fatal(err)
		}
		datum["id"] = datum["_id"]
		datum["CookTime"] = datum["cook_time"]
		datum["SourceUrl"] = datum["source_url"]
		data = append(data, datum)
	}
	err = mapstructure.Decode(data, &result)
	if err != nil {
		log.Fatal(err)
	}
}

func Upsert(collection string, doc interface{}) (string, error) {
	client, err := connect()
	col := client.Database(databaseName()).Collection(collection)

	var docId model.DocId
	mapstructure.Decode(doc, &docId)
	bDoc, err := bson.Marshal(doc)
	options := options.ReplaceOptions{Upsert: &[]bool{true}[0]}
	res, err := col.ReplaceOne(
		context.Background(),
		bson.D{{"_id", docId.ID}},
		bDoc,
		&options,
	)
	if err != nil {
		return "", err
		panic("omg fires: " + err.Error())
	}
	return fmt.Sprintf("%v", res.UpsertedID), nil
}
